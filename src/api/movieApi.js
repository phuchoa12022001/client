import axiosClient from './axiosClient';

const MovieApi = {
  getAll(params) {
    return axiosClient.get(`/movie?page_size=${params.page_size}&page=${params.page}`);
  },
  get(data) {
    return axiosClient.post('/auth/verify_token', data);
  },
  search(text) {
    return axiosClient.get(`http://localhost:3001/movie/search?q=${text}`);
  },
};

export default MovieApi;
