import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

import Search from "../components/search/search";
import SelectFlag from "../components/selectFlag/selectFlag";
import UserHeader from "../components/userHeader/userHeader";

const HeaderStyles = styled.div`
  border-bottom: 1px solid ${({ theme }) => theme.border.whiteDE} !important;
  .logo {
    font-size: 20px;
    font-weight: 700;
    color: ${({ theme }) => theme.colors.black};
  }
  .header__warpper {
    display: flex;
    height: 100%;
    align-items: center;
    justify-content: space-between;
    height: 70px;
  }
  .header__right {
    display: flex;
    align-items: center;
  }
  .Selectlanguage {
    margin-left: 10px;
    display: flex;
    justify-content: center;
    align-items: center;
    .ant-select-selection-item {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    img {
      height: 20px;
      margin: auto;
    }
  }
`;
function Header(props) {
  return (
    <HeaderStyles>
      <div className="container">
        <div className="header__warpper">
          <Link to="/">
            <div className="logo">XoaNen</div>
          </Link>
          <div className="header__right">
            <Search />
            <UserHeader />
            <SelectFlag />
          </div>
        </div>
      </div>
    </HeaderStyles>
  );
}

export default Header;
