import React from "react";
import styled from "styled-components";
import Menu from "./menu";

const SideBarStyles = styled.div`
  height: calc(100vh - 10px);
  overflow: auto;
  .sideBar__warpper {
    padding: 16px 0px;
  }
`;
function SideBar(props) {
  return (
    <SideBarStyles>
      <div className="sideBar__warpper">
        <Menu />
      </div>
    </SideBarStyles>
  );
}

export default SideBar;
