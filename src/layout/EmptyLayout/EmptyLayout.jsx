import React from "react";
import { Outlet } from "react-router-dom";

import Header from "../header";
function EmptyLayout(props) {
  return (
    <>
      <Header />
      <div className="main">
        <div className="container">
          <Outlet />
        </div>
      </div>
    </>
  );
}

export default EmptyLayout;
