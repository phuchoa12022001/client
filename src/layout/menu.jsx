import {
  GoogleOutlined,
  ContainerOutlined,
  DesktopOutlined,
  MailOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import { Menu } from "antd";
import styled from "styled-components";
import { Link } from "react-router-dom";
import ModaDark from "../components/modeDark/modeDark";
import { useSelector } from "react-redux";

function getItem(label, key, icon, children, type) {
  return {
    key,
    icon,
    children,
    label,
    type,
  };
}
const StyleIcon = {
  FontSize: 20,
};
const LinkItem = ({ to, text }) => {
  return (
    <Link to={to}>
      <span>{text}</span>
    </Link>
  );
};
const items = [
  getItem(
    <LinkItem text={"Trang chủ"} to={"/home"} />,
    "1",
    <HomeOutlined style={{ ...StyleIcon }} />
  ),
  getItem(
    <LinkItem text={"Phim mới"} to={"/news"} />,
    "2",
    <DesktopOutlined style={{ ...StyleIcon }} />
  ),
  getItem(
    <LinkItem text={"Phim Hot"} to={"/hots"} />,
    "3",
    <ContainerOutlined style={{ ...StyleIcon }} />
  ),
  getItem(
    <LinkItem text={"Thể loại"} to={"/category"} />,
    "sub1",
    <MailOutlined style={{ ...StyleIcon }} />,
    [
      getItem(<LinkItem text={"Thể loại 1"} to={"/category/theload1"} />, "5"),
      getItem(<LinkItem text={"Thể loại 2"} to={"/category/theload2"} />, "6"),
      getItem(<LinkItem text={"Thể loại 3"} to={"/category/theload3"} />, "7"),
      getItem(<LinkItem text={"Thể loại 4"} to={"/category/theload4"} />, "8"),
    ]
  ),
  getItem(
    <LinkItem text={"Liên hệ tôi"} to={"/contact"} />,
    "9",
    <GoogleOutlined style={{ ...StyleIcon }} />
  ),
];

const MenuStyles = styled.div`
  color: ${({ theme }) => theme.colors.black};
  .item {
    display: flex;
  }
`;
const MenuSideBar = () => {
  const themeState = useSelector((state) => state.Setting.setting.theme);
  return (
    <MenuStyles>
      <div
        style={{
          width: "100%",
        }}
      >
        <Menu
          theme={themeState}
          className="Menu"
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
          items={items}
        />
      </div>
      <ModaDark />
    </MenuStyles>
  );
};

export default MenuSideBar;
