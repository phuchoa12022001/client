import React from "react";
import { Row, Col } from "antd";
import { Outlet } from "react-router-dom";
import styled from "styled-components";

import Header from "../header";
import SideBar from "../sidebar";
const MainStyles = styled.div`
  .bg {
    background: ${({ theme }) => theme.bg.sidebar};
  }
`;
function MainLayout(props) {
  return (
    <MainStyles>
      <Header />
      <div className="main">
        <div className="container">
          <Row gutter={16}>
            <Col span={4} className="bg">
              <SideBar />
            </Col>
            <Col span={20}>
              <Outlet />
            </Col>
          </Row>
        </div>
      </div>
    </MainStyles>
  );
}

export default MainLayout;
