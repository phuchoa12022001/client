
export const LOCALE = {
  VIETNAMESE: 'vi',
  ENGLISH: 'en',
};

export const validateMessages = {
  [LOCALE.ENGLISH]: {
    required: 'is required',
  },
  [LOCALE.VIETNAMESE]: {
    required: 'bắt buộc',
  },
};
export const DEFAULT_PAGINATION = {
  PAGE: 1,
  LIMIT: 21,
};