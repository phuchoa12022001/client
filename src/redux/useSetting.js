import { createSlice } from '@reduxjs/toolkit';
export const useSetting = createSlice({
    name: "useSetting",
    initialState: {
        setting: {
            theme: "light"
        }
    },
    reducers: {
        ChangeTheme: (state, action) => {
            state.setting.theme = action.payload;
        },
    }
})
export const {
    ChangeTheme
} =
    useSetting.actions;
export default useSetting.reducer;