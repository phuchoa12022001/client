import { configureStore } from '@reduxjs/toolkit';

import Setting from './useSetting';
export default configureStore({
    reducer: {
        Setting
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false,
    }),
})