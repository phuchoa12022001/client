import React from "react";
import styled from "styled-components";

const ItemSelectFlagStyles = styled.div`
  display: flex;
  justify-content: center;
  cursor: pointer;
  .image {
    height: 40px;
    width: 20px;
    padding: 10px 0px;
  }
  :hover {
    opacity: 0.7;
  }
`;
function ItemSelectFlag({ name, img, onClick, index }) {
  return (
    <ItemSelectFlagStyles tabIndex="-1" onClick={() => onClick(index)} id={index}>
      <img src={img} alt={name} className="image" />
    </ItemSelectFlagStyles>
  );
}

export default ItemSelectFlag;
