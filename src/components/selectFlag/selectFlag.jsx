import { Input } from "antd";
import styled from "styled-components";
import React, { useEffect, useRef, useState } from "react";
import ItemSelectFlag from "./ItemSelectFlag";
import { DownOutlined, SearchOutlined } from "@ant-design/icons";
import FilterFlag from "../../utils/filterFlag";

const ListFlag = [
  {
    name: "Việt Nam",
    language: "vietnamese",
    code: "vn",
    img: "./assets/image/flag/vietnam.png",
  },
  {
    name: "Anh",
    language: "English",
    code: "en",
    img: "./assets/image/flag/unitedkingdom.png",
  },
];

const SelectFlagStyles = styled.div`
  position: relative;
  .search {
    width: 150px;
    color: ${({ theme }) => theme.colors.black};
  }
  .listSearch {
    width: 170px;
    z-index: 90;
    right: 0px;
    position: absolute;
    height: auto;
    background-color: ${({ theme }) => theme.bg.white};
    color: ${({ theme }) => theme.colors.black};
    flex-wrap: wrap;
    box-shadow: ${({ theme }) => theme.colors.search};
    border-radius: 0px 0px 5px 5px;
  }
  .Showflag {
    cursor: pointer;
    img {
      height: 20px;
      margin-right: 7px;
    }
  }
  .Boxicon {
    cursor: pointer;
    .icon {
      color: ${({ theme }) => theme.colors.selectflag};
    }
  }
  .BoxInput {
    display: flex;
    align-items: center;
    color: ${({ theme }) => theme.colors.black};
  }
`;
function SelectFlag(props) {
  const [input, setInput] = useState("");
  const [index, setIndex] = useState(0);
  const [open, setOpen] = useState(false);
  const [openList, setOpenList] = useState(false);
  const search = useRef();
  const NewList = FilterFlag(input, ListFlag);
  const handleClick = (index) => {
    setIndex(index);
    setOpen(false);
    setOpenList(false);
  };
  const handleChangeInput = (e) => {
    setInput(e.target.value);
  };
  const handleOpen = () => {
    setOpen(true);
    setOpenList(true);
  };
  const handleBlur = (e) => {
    if (e.relatedTarget) {
      handleClick(e.relatedTarget.id);
      return;
    }
    setOpen(false);
    setOpenList(false);
  };
  useEffect(() => {
    if (search.current) {
      search.current.focus();
    }
  }, [open]);
  return (
    <SelectFlagStyles>
      <div className="BoxInput">
        {!open ? (
          <div className="Showflag" onClick={handleOpen}>
            <img src={ListFlag[index].img} alt={ListFlag[index].code} />
          </div>
        ) : (
          <Input
            placeholder="Chọn ngôn ngữ"
            className="search"
            onChange={handleChangeInput}
            bordered={false}
            ref={search}
            value={input}
            onBlur={handleBlur}
          />
        )}
        <div className="Boxicon" onClick={handleOpen}>
          {open ? (
            <SearchOutlined style={{ fontSize: 12 }} className="icon" />
          ) : (
            <DownOutlined style={{ fontSize: 12 }} className="icon" />
          )}
        </div>
      </div>
      {openList && (
        <div className="listSearch">
          {NewList.map((language, index) => (
            <ItemSelectFlag
              {...language}
              onClick={handleClick}
              index={index}
              key={index}
            />
          ))}
        </div>
      )}
    </SelectFlagStyles>
  );
}

export default SelectFlag;
