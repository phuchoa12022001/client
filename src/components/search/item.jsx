import React from "react";
import styled from "styled-components";

const ItemStyles = styled.div`
  width: 100%;
  display: flex;
  padding: 10px 8px;
  a {
    display: flex;
  }
  .image {
    width: 100px;
    height: 80px;
  }
  .info {
    margin-left: 10px;
  }
  .name {
    font-size: 15px;
    line-height: 22px;
    color: ${({ theme }) => theme.colors.black};
  }
  .desc {
    font-size: 12px;
    line-height: 15px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    color: black;
    color: ${({ theme }) => theme.colors.black};
  }
`;
function ItemSearch(props) {
  return (
    <ItemStyles>
      <a href="!">
        <img src="./assets/image/naruto.jpg" alt="naruto" className="image" />
        <div className="info">
          <h3 className="name">Naruto</h3>
          <p className="desc">
            Tập phim này xoay quanh nhiệm vụ do Naruto, Kakashi, Sakura, và Lee
            được cử bảo vệ một hoàng tử trong chuyến đi vòng quanh thế giới
          </p>
        </div>
      </a>
    </ItemStyles>
  );
}

export default ItemSearch;
