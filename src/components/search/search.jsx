import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { SearchOutlined } from "@ant-design/icons";
import clsx from "clsx";
import { useIntl } from "react-intl";

import ItemSearch from "./item";
import useDebouonce from "../../hooks/useDebouonce";
const SearchStyles = styled.div`
  .BoxSearch {
    padding: 4px 8px;
    position: relative;
    z-index: 90;
    color: ${({ theme }) => theme.colors.black};
  }
  .search {
    padding: 4px 8px;
    border: none;
    outline: none;
    font-size: 14px;
    line-height: 22px;
    width: 100px;
    transition: 0.4s all ease-out;
    font-weight: 700;
    border-radius: 5px;
    background: ${({ theme }) => theme.bg.white};
    color: ${({ theme }) => theme.colors.black};
  }
  .active .search {
    width: 250px;
  }
  .active .BoxResult {
    display: block;
  }
  .BoxResult {
    position: absolute;
    width: 450px;
    right: 0px;
    opacity: 0;
    display: none;
    height: 300px;
    animation: BoxResult 0.4s ease-out;
    animation-delay: 0.4s;
    animation-fill-mode: forwards;
    justify-content: center;
    background-color: ${({ theme }) => theme.bg.white};
    color: ${({ theme }) => theme.colors.black};
    overflow: auto;
    flex-wrap: wrap;
    box-shadow: ${({ theme }) => theme.colors.search};
    .text {
      font-size: 13px;
      line-height: 20px;
      width: 100%;
      text-align: center;
      font-weight: 500;
    }
  }
  @keyframes BoxResult {
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  .list {
    display: flex;
    flex-wrap: wrap;
  }
  @media only screen and (max-width: 600px) {
    .BoxResult {
      width: 90vw;
    }
  }
`;

function Search(props) {
  const [active, setActive] = useState(false);
  const [search, setSearch] = useState("");
  const intl = useIntl();
  const [searchApi, setSearchApi] = useState("");
  const { value, loading } = useDebouonce(search, 1000);
  const handleFocus = () => {
    setActive(true);
  };
  const handleBlur = () => {
    setActive(false);
  };
  const HandleChange = (e) => {
    setSearch(e.target.value);
    // SetSearchApi(debounced)
  };
  useEffect(() => {
    setSearchApi(value);
  }, [value]);
  return (
    <SearchStyles>
      <div
        className={clsx("BoxSearch", {
          active: active,
        })}
      >
        <SearchOutlined />
        <input
          onBlur={handleBlur}
          type="text"
          onFocus={handleFocus}
          placeholder={intl.formatMessage({ id: `search.placeholder` })}
          className={clsx("search")}
          value={search}
          onChange={HandleChange}
        />
        <div className="Loading"></div>
        <div className="BoxResult">
          <p className="text">
            search :{loading ? "đang tìm kiếm..." : searchApi}
          </p>
          <div className="list">
            <ItemSearch />
            <ItemSearch />
            <ItemSearch />
            <ItemSearch />
            <ItemSearch />
            <ItemSearch />
            <ItemSearch />
          </div>
        </div>
      </div>
    </SearchStyles>
  );
}

export default Search;
