import React from "react";
import { Button } from "antd";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";

const UserStyles = styled.div`
  margin-right: 10px;
  .btn {
    background-color: ${({ theme }) => theme.bg.buttonAuth};
    color: ${({ theme }) => theme.colors.black};
    .icon {
      margin-left: 0px;
    }
  }
`;
function UserHeader(props) {
  return (
    <UserStyles>
      <Link to={"/login"}>
        <Button className="btn">
          {" "}
          <UserOutlined className="icon" />
          Đăng nhập | Đăng ký
        </Button>
      </Link>
    </UserStyles>
  );
}

export default UserHeader;
