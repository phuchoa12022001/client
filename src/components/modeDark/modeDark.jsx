import React from "react";
import styled from "styled-components";
import { MoonIcon, SunIcon } from "../../assets/icon";
import { useDispatch, useSelector } from "react-redux";
import { ChangeTheme } from "../../redux/useSetting";
import clsx from "clsx";

const DarkStyles = styled.div`
  .ModeDark {
    margin-top: 10px;
    width: 98%;
  }
  .BoxMode {
    background: ${({ theme }) => theme.bg.demodark};
    border-radius: 10px;
    padding: 2px;
    display: flex;
  }
  .text {
    font-weight: 700;
    margin-bottom: 10px;
    font-size: 14px;
  }
  .boxIcon {
    display: flex;
    font-weight: 500;
    align-items: center;
    padding: 7px 0px;
    font-size: 14px;
    line-height: 20px;
    width: 50%;
    display: flex;
    justify-content: center;
    cursor: pointer;
    .icon {
      width: 16px;
    }
    p {
      margin-left: 5px;
    }
  }
  .boxIcon:hover {
    .icon {
      width: 20px;
    }
  }
  .boxIcon.active {
    background: ${({ theme }) => theme.bg.white};
    border-radius: 10px;
    .icon {
      width: 20px;
    }
  }
`;
function ModeDark(props) {
  const dispatch = useDispatch();
  const themeState = useSelector((state) => state.Setting.setting.theme);
  const handleThemeDark = () => {
    dispatch(ChangeTheme("dark"));
  };
  const handleThemeLight = () => {
    dispatch(ChangeTheme("light"));
  };
  return (
    <DarkStyles>
      <div className="ModeDark">
        <p className="text">Chế độ :</p>
        <div className="BoxMode">
          <div
            onClick={handleThemeLight}
            className={clsx("boxIcon", {
              active: themeState === "light",
            })}
          >
            <SunIcon className="icon" />
            <p>Sáng</p>
          </div>
          <div
            onClick={handleThemeDark}
            className={clsx("boxIcon", {
              active: themeState === "dark",
            })}
          >
            <MoonIcon className="icon" />
            <p>Tối</p>
          </div>
        </div>
      </div>
    </DarkStyles>
  );
}

export default ModeDark;
