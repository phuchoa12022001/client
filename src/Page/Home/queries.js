import { useQuery } from 'react-query';
import MovieApi from '../../api/movieApi';

export const useMovie = ({ options, params }) => {

    return useQuery({
        queryKey: params,
        queryFn: () => MovieApi.getAll(params),
        ...options,
    });
};