import React from "react";
import styled from "styled-components";
import ListUpdate from "./ListUpdate";
const HomeStyles = styled.div`
  .Home__warpper {
    padding: 8px;
    padding-top: 20px;
  }
  .title {
    font-weight: 700;
    margin-top: 10px;
    margin-bottom: 20px;
    color: ${({ theme }) => theme.colors.black};
  }
`;
function Home(props) {
  return (
    <HomeStyles>
      <div className="Home__warpper">
        <ListUpdate />
      </div>
    </HomeStyles>
  );
}

export default Home;
