
import { createGlobalStyle } from 'styled-components';

const LightTheme = {
  colors: {
    black: "#000",
    white: "#fff",
    selectflag: "#00000040",
  },
  bg: {
    white: "#fff",
    demodark: "#D8DADF",
    sidebar: "#fff",
    scrollbar: "rgba(0, 0, 0, .15)",
    scrollbarTrack: "rgba(0, 0, 0, 0)",
    buttonAuth: "#fff",
  },
  border: {
    whiteDE: "#dee2e6",
  },
  boxShadow: {
    search: "0 0.5rem 1rem rgb(0 0 0 / 15%)"
  }
}
const DarkTheme = {
  colors: {
    black: "#fff",
    white: "#000",
    selectflag: "#fff"
  },
  bg: {
    white: "#000",
    demodark: "#303036",
    sidebar: "#001529",
    scrollbar: "rgba(255, 255, 255, .15)",
    scrollbarTrack: "rgba(255, 255, 255, 0)",
    buttonAuth: "#000C17",
  },
  border: {
    whiteDE: "black",
  },
  boxShadow: {
    search: "0 0.5rem 1rem rgb(255 255 255 / 15%)"
  }
}

export const theme = {
  light: LightTheme,
  dark: DarkTheme
};


export const GlobalStyle = createGlobalStyle`

@import url('https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,300;0,400;0,500;0,600;0,700;0,900;1,300;1,400;1,800&display=swap');

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
body { 
  font-family: 'Raleway', sans-serif;
  font-weight: 500;
}
html *::-webkit-scrollbar {
  border-radius: 0;
  width: 8px;
}
html *::-webkit-scrollbar-thumb {
  border-radius: 4px;
  background-color: ${({ theme }) => theme.bg.scrollbar};
}
html *::-webkit-scrollbar-thumb {
  border-radius: 4px;
  background-color: ${({ theme }) => theme.bg.scrollbar};
}
html *::-webkit-scrollbar-track {
  border-radius: 0;
  background-color: ${({ theme }) => theme.bg.scrollbarTrack};
}
html *::-webkit-scrollbar-track {
  border-radius: 0;
  background-color: ${({ theme }) => theme.bg.scrollbarTrack};
}
`