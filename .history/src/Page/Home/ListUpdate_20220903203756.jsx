import React from "react";
import { Row } from "antd";
import { Pagination } from "antd";

import { useMovie } from "./queries";
import ItemUpdate from "./ItemUpdate";
import { useCustomSearchParams } from "../../hooks/useCustomSearchParams";

function ListUpdate(props) {
  const [search, setSearch] = useCustomSearchParams();
  const handleChange = (value) => {
    setSearch({ ...search, page: value, page_size: 21 });
  };
  const argument = {
    params: search,
    options: {
      keepPreviousData: true,
    },
  };
  const { data, isLoading, isFetched } = useMovie(argument);
  return (
    <>
      {!isLoading && isFetched ? (
        <>
          <Row gutter={16}>
            {data.data.data.map((item, index) => (
              <ItemUpdate key={index} {...item} />
            ))}
          </Row>
          <Pagination
            defaultCurrent={+data?.data.meta.current_page}
            pageSize={21}
            total={data?.data.meta.total}
            showSizeChanger={false}
            showQuickJumper
            onChange={handleChange}
            showTotal={(total) => `Tổng cộng ${total} Bộ Phim`}
          />
        </>
      ) : (
        false
      )}
    </>
  );
}

export default ListUpdate;
