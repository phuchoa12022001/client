import React from "react";
import { Col } from "antd";
import styled from "styled-components";

const ItemUpdatetyles = styled.div`
  position: relative;
  .image {
    width: 100%;
    max-height: 500px;
  }
  .name {
    position: absolute;
    width: 100%;
    padding: 20px 0px;
    background: ${({ theme }) => theme.colors.white};
    bottom: 0px;
    text-align: center;
    font-size: 16px;
    line-height: 20px;
    font-weight: 600;
    color: ${({ theme }) => theme.colors.black};
  }
  .episode_total {
    position: absolute;
    right: 20px;
    top: 10px;
    padding: 15px 10px;
    background: ${({ theme }) => theme.colors.white};
    border-radius: 10px;
    color: ${({ theme }) => theme.colors.black};
  }
  margin-bottom: 8px;
`;
function ItemUpdate({ name, thumb_url, episode_current, episode_total }) {
  return (
    <Col span={8}>
      <ItemUpdatetyles>
        <h3 className="name">{name}</h3>
        <img src={thumb_url} alt={name} className="image" />
        <h3 className="episode_total">
          {episode_current}{" "}
          <span>{episode_total ? "/" + episode_total : ""}</span>
        </h3>
      </ItemUpdatetyles>
    </Col>
  );
}

export default ItemUpdate;
