import styled, { ThemeProvider } from 'styled-components';
import { IntlProvider } from 'react-intl';
import { Routes, Route } from 'react-router-dom';
import { ConfigProvider } from 'antd';
import moment from 'moment';
import flatten from 'flat';
import { useSelector } from 'react-redux';
import { ErrorBoundary } from 'react-error-boundary'
import { QueryClient, QueryClientProvider } from 'react-query';

// Layout 
import MainLayout from './layout/MainLayout/MainLayout';
import EmptyLayout from './layout/EmptyLayout/EmptyLayout';

import { theme, GlobalStyle } from './styles/GlobalStyle';

// MainLayout 
import Home from './Page/Home/Home';

// EmptyLayout 
import EmptyPage from './Page/EmptyPage';

import { validateMessages } from './constants/common';
import AppLocale from './config/translations';


import 'antd/dist/antd.min.css'

const AppStyles = styled.div` 
width : 100%;
  .main {
    width : 100%
  }
  .container {
    max-width : 1470px;
    padding : 0px 15px;
  }
  .warpper {
    max-width :100%;
    margin : 0 auto;
    background: ${({ theme }) => theme.bg.white};
  }
`
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
  },
});
function ErrorFallback({ error, resetErrorBoundary }) {
  console.log(error.name);    // CustomError
  console.log(error.foo);     // baz
  console.log(error.message); // bazMessage
  console.log(error.stack);  
  console.log(error.fileName); // stacktrace
  return (
    <div role="alert">
      <p>{error.stack}</p>
      <pre>{error.message}</pre>
      <button onClick={resetErrorBoundary}>Try again</button>
    </div>
  )
}

function App() {
  const locale = 'vi';
  const themeState = useSelector(state => state.Setting.setting.theme);
  moment.locale(locale);
  return (
    <ConfigProvider
      locale={AppLocale[locale].antd}
      form={{ validateMessages: validateMessages[locale] }}
    >
      <ErrorBoundary FallbackComponent={ErrorFallback}>
        <home></home>
        <QueryClientProvider client={queryClient}>
          <IntlProvider
            locale={locale}
            messages={flatten(AppLocale[locale].messages)}
          >
            <ThemeProvider theme={theme[themeState]}>
              <GlobalStyle />
              <AppStyles>
                <div className="warpper">
                  <div className="App">
                    <Routes>
                      <Route element={<MainLayout />}>
                        <Route path="/" element={<Home />} />
                      </Route>
                      <Route element={<EmptyLayout />}>
                        <Route path="/emptyLayout" element={<EmptyPage />} />
                        <Route path="*" element={<p>404</p>} />
                      </Route>
                    </Routes>
                  </div>
                </div>
              </AppStyles>
            </ThemeProvider>
          </IntlProvider>
        </QueryClientProvider>
      </ErrorBoundary>
    </ConfigProvider>
  );
}

export default App;
